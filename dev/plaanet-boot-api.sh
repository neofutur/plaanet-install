echo '***********************************'
echo 'LETS TRY TO INSTALL THIS PLAANET API !'
echo '***********************************'
echo ''
echo '>>> IMPORTANT : before to run this script : replace all "johnny" by your own username'
echo '>>> AND modify all path as needed by YOUR system'
echo ''

echo '### install plaanet'
mkdir /home/johnny/Node
cd /home/johnny/Node

echo '### clone plaanet-api'
git clone https://gitlab.com/plaanet/plaanet-api.git
cd plaanet-api
sudo cp ./config/default.json ./config/development.json
sudo mkdir ./public/uploads/avatar
sudo chown johnny ./public/uploads/avatar
sudo chown johnny ./public/uploads/post

echo '### npm install plaanet-api'
sudo npm install

echo '***********'
echo 'BOOT DONE !'
echo '***********'
echo ''
echo '> TO START API, USE FOLLOWING COMMANDES :'
echo 'cd /home/johnny/Node/plaanet-api'
echo 'node main.js'
echo '>> then API is running on http://localhost:3000'