echo '***********************************'
echo 'LETS TRY TO INSTALL THIS PLAANET-BERRY !'
echo '***********************************'

echo '### apt-get update'
sudo apt-get update

echo '### install git'
sudo apt-get install -y git-all

echo '### install npm'
sudo apt-get install -y npm
sudo npm i npm@latest -g

echo '### install plaanet'
mkdir /home/ubuntu/Node
cd /home/ubuntu/Node

echo '### clone plaanet-client'
cd /home/ubuntu/Node
git clone https://gitlab.com/plaanet/plaanet-client.git
cd plaanet-client

echo '### npm install plaanet-client'
sudo cp /home/ubuntu/plaanet-install/prod/config-client-default.json ./src/config/default.json
sudo npm install

echo '### npm build plaanet-client'
sudo npm install -g serve
sudo npm run build

echo '### create services for production'
sudo cp /home/ubuntu/plaanet-install/prod/plaanet-client.service /etc/systemd/system/plaanet-client.service

sudo chmod 755 /etc/systemd/system/plaanet-client.service

sudo systemctl enable plaanet-client

echo '***********'
echo 'BOOT DONE !'
echo '***********'
echo '> READY TO REBOOT'
echo '(you must change plaanet-client/config to set specifics parameters of your node (IP:PORT))'