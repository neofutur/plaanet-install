echo '***********************************'
echo 'LETS TRY TO INSTALL THIS PLAANET-BERRY !'
echo '***********************************'

echo '### apt-get update'
sudo apt-get update

echo '### install mongo'
sudo apt-get install -y mongodb-server-core
sudo apt-get install -y mongodb-clients
sudo mkdir /data
sudo mkdir /data/db
sudo chown -R ubuntu:ubuntu /data

mkdir /home/ubuntu/logs
mkdir /home/ubuntu/logs/mongo

echo '### install git'
sudo apt-get install -y git-all

echo '### install npm'
sudo apt-get install -y npm
sudo npm i npm@latest -g

echo '### install plaanet'
mkdir /home/ubuntu/Node
cd /home/ubuntu/Node

echo '### clone plaanet-api'
git clone https://gitlab.com/plaanet/plaanet-api.git
cd plaanet-api
sudo cp /home/ubuntu/plaanet-install/prod/config-api-default.json ./config/default.json
sudo mkdir ./public/uploads/avatar
sudo chown ubuntu ./public/uploads/avatar
sudo chown ubuntu ./public/uploads/post
echo '### npm install plaanet-api'
sudo npm install

echo '### create services for production'
sudo cp /home/ubuntu/plaanet-install/prod/plaanet-api.service /etc/systemd/system/plaanet-api.service
sudo cp /home/ubuntu/plaanet-install/prod/mongo.service /etc/systemd/system/mongo.service

sudo chmod 755 /etc/systemd/system/plaanet-api.service
sudo chmod 755 /etc/systemd/system/mongo.service

sudo systemctl enable plaanet-api
sudo systemctl enable mongo

echo '***********'
echo 'BOOT DONE !'
echo '***********'
echo '> READY TO REBOOT'
echo '(you must change plaanet-api/config to set specifics parameters of your node (IP:PORT))'